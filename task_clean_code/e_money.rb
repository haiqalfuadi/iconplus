# frozen_string_literal: true

require 'faker'
# Main class
class EMoney
  attr_accessor :id
  attr_accessor :saldo
  attr_accessor :status
  def initialize
    @id = Faker::Number.number(digits: 10)
    @saldo = 0
    @status = 0
  end

  def aktivasi
    if @status.zero?
      @status = 1
      puts 'Aktivasi Berhasil'
    else
      puts 'Sudah Aktif'
    end
  end

  def cek_status
    if @status.zero?
      false
    else
      true
    end
  end

  def cek_saldo
    puts "Saldo : #{@saldo}"
  end

  def top_up_saldo(amount)
    if cek_status
      @saldo += amount
      puts "Top Up berhasil, jumlah saldo sekarang = #{@saldo}"
    else
      puts 'E-Money harus diaktifkan terlebih dahulu'
    end
  end

  def pengurangan_saldo(amount)
    if cek_status
      if @saldo >= amount
        @saldo -= amount
        puts "Saldo saat ini : #{@saldo}"
      else
        puts 'Saldo Anda tidak mencukupi!'
      end
    else
      puts 'Anda harus melakukan aktivasi terlebih dahulu!'
    end
  end

  def transfer_saldo(recipient, amount)
    if recipient.cek_status
      if @saldo >= amount
        @saldo -= amount
        recipient.saldo += amount
        puts 'Transaksi berhasil !'
        puts "Saldo anda saat ini : #{@saldo}"
        puts "Saldo recipient : #{recipient.saldo}"
      else
        puts 'Saldo Anda tidak mencukupi!'
        puts "Saldo anda saat ini : #{@saldo}"
      end
    else
      puts 'E-Money recipient harus diaktifkan terlebih dahulu!'
    end
  end
end
