names = ['Ahmad', 'Tono', 'Tini', 'Bambang', 'Agus', 'Agung']

names.each { |n| 
    if (n.length % 2 == 1) && (n[0,1] == 'A')
        puts n.upcase
    elsif n[0,1] == 'T'
        puts n.downcase
    else
        puts n.reverse.capitalize
    end    

}

